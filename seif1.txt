.open magshimim.db

create table StudentsInClasses(studentID INT, classID INT);

create table Teachers(teacherID INT, teacherName TEXT);

create table Categories(catID INT, catName TEXT);

create table Classes(classID INT, className TEXT, teacherID INT, catID INT);

create table Students(studentID INT, studentName TEXT, studentAge TEXT, studentGrade TEXT);

